//Esta función hace que los numeros aparezcan en la pantalla. Permite la concatenación.
function print(value){

    var operant = document.getElementById("entryNumber").innerHTML;
    var operator = ['+', '-', '*', '/'];

    if(operant == ""){
        document.getElementById("entryNumber").innerHTML = value;

    }else{

        operant += value;
        document.getElementById("entryNumber").innerHTML = operant;
    }

    if(operator.includes(value)){
        document.getElementById("exitNumber").innerHTML = operant;
        document.getElementById("entryNumber").innetHTML = " ";
    }

    // Limitación del cero
    if(value == "0" && operant.substring(0 , 1) == "0" && operant.substring(1, 2) != "."){ 
        var operant = operant.substring(0 , operant.length - 1);
        document.getElementById("entryNumber").innerHTML = operant;
    }

    if(operant.substring(0,2) != "0."){
        if(value != "0" && operant.substring(0 , 1) == "0"){
            var substitution = operant.replace("0", value);
            var operant = operant.substring(1);
            document.getElementById("entryNumber").innerHTML = operant;  
        }
    }

    // Limitación  del punto
    if(value == "." && operant.substring(0 , 1)== ""){
        document.getElementById("entryNumber").innerHTML = "0.";
    }
}

//Función que hace que el botón de = funcione adecuadamente. Se ha usado un eval() para evitar escribir 
//las funciones de cada boton de operación.
function calculate(value){

    var operation = document.getElementById("entryNumber").innerHTML;

    var result = eval(operation);

    document.getElementById("entryNumber").innerHTML = result;

}

//Función que hace que el botón de retroceso "←" funcione correctamente.
//Con este botón se elimina el último número escrito. si hay un cero, no lo borra y si 
//la pantalla se queda sin numeros, automaticamente se pone un cero.
function retroceso(){

    var str = document.getElementById("entryNumber").innerHTML;

    var delet = str.slice(0, -1);

    document.getElementById("entryNumber").innerHTML = delet;

    if(document.getElementById("entryNumber").innerHTML == ""){
        document.getElementById("entryNumber").innerHTML = "0";
    }
}

//Función que hace que el botón "C" funcione.
//Borra todos los elementos de la calculadora y se pone un cero.
//Negación: se pone un "!".
function borrar(){

    if(!document.getElementById("entryNumber").innerHTML == "0"){
        document.getElementById("entryNumber").innerHTML = "0";
        document.getElementById("exitNumber").innerHTML = "0";
    }
}

//Función que hace que el botón "CE" funcione. Con este botón solo se elimina
//lo que aparece en la pantalla de "entryNumber".
 function limpiador(){

    if(document.getElementById("entryNumber").innerHTML != "0"){
        document.getElementById("entryNumber").innerHTML = "0";
    }
 }

